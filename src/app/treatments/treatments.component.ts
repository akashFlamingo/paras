import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-treatments',
  templateUrl: './treatments.component.html',
  styleUrls: ['./treatments.component.css']
})
export class TreatmentsComponent implements OnInit {

  customTitle:any = {};

  constructor() { }

  ngOnInit(): void {
    this.customTitle = "Treatments And Procedures";
  }

}
