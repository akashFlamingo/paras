import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-our-hospitals',
  templateUrl: './our-hospitals.component.html',
  styleUrls: ['./our-hospitals.component.css']
})
export class OurHospitalsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  netHospital: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    margin:40,
    dots: false,
    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      1000: {
        items: 3
      },
      1100: {
        items: 4
      }
    },
    nav: true
  }

}
