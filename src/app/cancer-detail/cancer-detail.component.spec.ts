import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CancerDetailComponent } from './cancer-detail.component';

describe('CancerDetailComponent', () => {
  let component: CancerDetailComponent;
  let fixture: ComponentFixture<CancerDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CancerDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CancerDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
