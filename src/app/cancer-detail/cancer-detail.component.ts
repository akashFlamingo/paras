import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cancer-detail',
  templateUrl: './cancer-detail.component.html',
  styleUrls: ['./cancer-detail.component.css']
})
export class CancerDetailComponent implements OnInit {

  constructor() { }

  customTitle: any = {};
  customImage:any = {};
  customColor: any = {}

  ngOnInit(): void {
    this.customTitle = "Brain Cancer"
    this.customImage = "assets/images/1cac8584c1940f21d8ecd8258ee90741.png"
    this.customColor = "#652F9E"

    
  }

}
