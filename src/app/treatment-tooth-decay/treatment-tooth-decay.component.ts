import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-treatment-tooth-decay',
  templateUrl: './treatment-tooth-decay.component.html',
  styleUrls: ['./treatment-tooth-decay.component.css']
})
export class TreatmentToothDecayComponent implements OnInit {

  constructor() { }

  customTitle: any = {};
  customImage:any = {};

  ngOnInit(): void {
    this.customTitle = "Tooth Decay"
    this.customImage = "assets/images/tooth.png"

    
  }}