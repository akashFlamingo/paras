import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TreatmentToothDecayComponent } from './treatment-tooth-decay.component';

describe('TreatmentToothDecayComponent', () => {
  let component: TreatmentToothDecayComponent;
  let fixture: ComponentFixture<TreatmentToothDecayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TreatmentToothDecayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TreatmentToothDecayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
