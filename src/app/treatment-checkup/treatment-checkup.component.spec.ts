import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TreatmentCheckupComponent } from './treatment-checkup.component';

describe('TreatmentCheckupComponent', () => {
  let component: TreatmentCheckupComponent;
  let fixture: ComponentFixture<TreatmentCheckupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TreatmentCheckupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TreatmentCheckupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
