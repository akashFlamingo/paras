import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutIdeologyComponent } from './about-ideology.component';

describe('AboutIdeologyComponent', () => {
  let component: AboutIdeologyComponent;
  let fixture: ComponentFixture<AboutIdeologyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AboutIdeologyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutIdeologyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
