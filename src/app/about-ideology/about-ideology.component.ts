import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about-ideology',
  templateUrl: './about-ideology.component.html',
  styleUrls: ['./about-ideology.component.css']
})
export class AboutIdeologyComponent implements OnInit {

  constructor() { }

  customTitle: any = {};

  ngOnInit(): void {
    this.customTitle = "Our Ideology"
  }

}