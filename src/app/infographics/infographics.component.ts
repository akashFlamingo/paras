import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-infographics',
  templateUrl: './infographics.component.html',
  styleUrls: ['./infographics.component.css']
})
export class InfographicsComponent implements OnInit {

  
  constructor() { }

  customTitle: any = {};

  ngOnInit(): void {
    this.customTitle = "Infographics & Helpful Videos"
  }

}
