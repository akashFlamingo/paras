import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-cancer-home-page',
  templateUrl: './cancer-home-page.component.html',
  styleUrls: ['./cancer-home-page.component.css']
})
export class CancerHomePageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  cancerTreatment: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    margin:30,
    dots: false,
    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      1000: {
        items: 4
      },
      1100: {
        items: 4
      }
    },
    nav: true
  }
  cancerAdd: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    margin:30,
    dots: false,
    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      1000: {
        items: 1
      },
      1100: {
        items: 1
      }
    },
    nav: true
  }
}



