import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CancerHomePageComponent } from './cancer-home-page.component';

describe('CancerHomePageComponent', () => {
  let component: CancerHomePageComponent;
  let fixture: ComponentFixture<CancerHomePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CancerHomePageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CancerHomePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
