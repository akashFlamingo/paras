import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoeDoctorsComponent } from './coe-doctors.component';

describe('CoeDoctorsComponent', () => {
  let component: CoeDoctorsComponent;
  let fixture: ComponentFixture<CoeDoctorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoeDoctorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoeDoctorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
