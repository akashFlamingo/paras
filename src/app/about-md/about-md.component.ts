import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about-md',
  templateUrl: './about-md.component.html',
  styleUrls: ['./about-md.component.css'],
})
export class AboutMdComponent implements OnInit {

  constructor() { }
  customTitle: any = {};

  ngOnInit(): void {
    this.customTitle = 'Managing Director’s Message';
  }
}
