import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cancer-survior-story',
  templateUrl: './cancer-survior-story.component.html',
  styleUrls: ['./cancer-survior-story.component.css']
})
export class CancerSurviorStoryComponent implements OnInit {

  constructor() { }
  customTitle: any = {};
  customImage:any = {};
  customColor: any = {}
  customTextColor:any = {};
  
  
  

  ngOnInit(): void {
    this.customTitle = "Survior Story"
    this.customImage = "assets/images/female.png"
    this.customColor = "#652F9E"
    this.customTextColor = "#ffffff"
  
   
  }

}
