import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { OwlModule } from 'ngx-owl-carousel';
import { DocotorsListingComponent } from './docotors-listing/docotors-listing.component';
import { TitleComponent } from './title/title.component';
import { AboutTeamComponent } from './about-team/about-team.component';
import { DepartmentsComponent } from './departments/departments.component';
import { TitleBannerComponent } from './title-banner/title-banner.component';
import { DoctorsDetailComponent } from './doctors-detail/doctors-detail.component';
import { AboutIdeologyComponent } from './about-ideology/about-ideology.component';
import { OurHospitalsComponent } from './our-hospitals/our-hospitals.component';
import { CoeComponent } from './coe/coe.component';
import { CoeHospitalsComponent } from './coe-hospitals/coe-hospitals.component';
import { AboutGroupCompaniesComponent } from './about-group-companies/about-group-companies.component';
import { PatientLoginComponent } from './patient-login/patient-login.component';
import { CancerHomePageComponent } from './cancer-home-page/cancer-home-page.component';
import { CoeSliderComponent } from './coe-slider/coe-slider.component';
import { CancerDetailComponent } from './cancer-detail/cancer-detail.component';
import { CancerSurviorStoryComponent } from './cancer-survior-story/cancer-survior-story.component';
import { OurAchivementComponent } from './our-achivement/our-achivement.component';
import { AboutMdComponent } from './about-md/about-md.component';
import { BlogListingComponent } from './blog-listing/blog-listing.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { TreatmentToothDecayComponent } from './treatment-tooth-decay/treatment-tooth-decay.component';
import { SpecialitesComponent } from './specialites/specialites.component';
import { TreatmentsComponent } from './treatments/treatments.component';
import { CoeDoctorsComponent } from './coe-doctors/coe-doctors.component';
import { PatientTestimonialComponent } from './patient-testimonial/patient-testimonial.component';
import { CoeFaqComponent } from './coe-faq/coe-faq.component';
import { HospitalPageComponent } from './hospital-page/hospital-page.component';
import { ParasHealthAppComponent } from './paras-health-app/paras-health-app.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { DirectionsComponent } from './directions/directions.component';
import { SpecialClinicComponent } from './special-clinic/special-clinic.component';
import { TreatmentCheckupComponent } from './treatment-checkup/treatment-checkup.component';
import { TreatmentChildHealthCheckupComponent } from './treatment-child-health-checkup/treatment-child-health-checkup.component';
import { FaqComponent } from './faq/faq.component';
import { InfographicsComponent } from './infographics/infographics.component';
import { InfographicsVideoComponent } from './infographics-video/infographics-video.component';
import { ContactUsComponent } from './contact-us/contact-us.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    DocotorsListingComponent,
    TitleComponent,
    AboutTeamComponent,
    DepartmentsComponent,
    TitleBannerComponent,
    DoctorsDetailComponent,
    AboutIdeologyComponent,
    OurHospitalsComponent,
    CoeComponent,
    CoeHospitalsComponent,
    AboutGroupCompaniesComponent,
    PatientLoginComponent,
    CancerHomePageComponent,
    CoeSliderComponent,
    CancerDetailComponent,
    CancerSurviorStoryComponent,
    OurAchivementComponent,
    AboutMdComponent,
    BlogListingComponent,
    BlogDetailComponent,
    TreatmentToothDecayComponent,
    SpecialitesComponent,
    TreatmentsComponent,
    CoeDoctorsComponent,
    PatientTestimonialComponent,
    CoeFaqComponent,
    ParasHealthAppComponent,
    ButtonsComponent,
    HospitalPageComponent,
    DirectionsComponent,
    SpecialClinicComponent,
    TreatmentCheckupComponent,
    TreatmentChildHealthCheckupComponent,
    FaqComponent,
    InfographicsComponent,
    InfographicsVideoComponent,
    ContactUsComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CarouselModule,
    OwlModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
