import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientTestimonialComponent } from './patient-testimonial.component';

describe('PatientTestimonialComponent', () => {
  let component: PatientTestimonialComponent;
  let fixture: ComponentFixture<PatientTestimonialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientTestimonialComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientTestimonialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
