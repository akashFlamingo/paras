import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutTeamComponent } from './about-team/about-team.component';
import { DepartmentsComponent } from './departments/departments.component';
import { DocotorsListingComponent } from './docotors-listing/docotors-listing.component';
import { DoctorsDetailComponent } from './doctors-detail/doctors-detail.component';
import { HomeComponent } from './home/home.component';
import  { AboutIdeologyComponent } from './about-ideology/about-ideology.component'
import { CoeComponent } from './coe/coe.component';
import { AboutGroupCompaniesComponent} from './about-group-companies/about-group-companies.component';
import{ PatientLoginComponent } from './patient-login/patient-login.component';
import {CancerHomePageComponent}from './cancer-home-page/cancer-home-page.component';
import { CancerDetailComponent } from './cancer-detail/cancer-detail.component';;
import{ CancerSurviorStoryComponent } from './cancer-survior-story/cancer-survior-story.component';
import{ OurAchivementComponent } from './our-achivement/our-achivement.component';
import{ AboutMdComponent } from './about-md/about-md.component';
import { BlogListingComponent} from './blog-listing/blog-listing.component';
import { BlogDetailComponent} from './blog-detail/blog-detail.component';
import { TreatmentToothDecayComponent} from './treatment-tooth-decay/treatment-tooth-decay.component';
import {SpecialitesComponent} from './specialites/specialites.component';
import {TreatmentsComponent} from './treatments/treatments.component';
import { ParasHealthAppComponent } from './paras-health-app/paras-health-app.component';
import {HospitalPageComponent}from './hospital-page/hospital-page.component';
import  {ButtonsComponent}from './buttons/buttons.component';
import {DirectionsComponent}from './directions/directions.component';
import {SpecialClinicComponent}from './special-clinic/special-clinic.component';
import {TreatmentCheckupComponent}from './treatment-checkup/treatment-checkup.component';
import {TreatmentChildHealthCheckupComponent} from './treatment-child-health-checkup/treatment-child-health-checkup.component';
import {FaqComponent}from './faq/faq.component';
import {InfographicsComponent}from './infographics/infographics.component';
import {InfographicsVideoComponent}from './infographics-video/infographics-video.component';
import {ContactUsComponent}from './contact-us/contact-us.component'

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'doctors-listing', component: DocotorsListingComponent },
  { path: 'about-team', component: AboutTeamComponent },
  { path: 'coe/neurology', component: CoeComponent },
  { path: 'doctors-detail', component: DoctorsDetailComponent },
  { path: 'about-ideology', component: AboutIdeologyComponent },
  {path: 'about-group-companies', component: AboutGroupCompaniesComponent},
  {path: 'patient-login', component: PatientLoginComponent},
  {path: 'cancer-home-page',component: CancerHomePageComponent},
  {path: 'cancer-detail', component: CancerDetailComponent},
  {path: 'cancer-survior-story', component: CancerSurviorStoryComponent},
  {path: 'our-achivement', component: OurAchivementComponent},
  {path: 'about-md', component: AboutMdComponent},
  {path: 'blogs', component: BlogListingComponent},
  {path: 'blog-detail', component: BlogDetailComponent},
  {path: 'treatments', component: TreatmentsComponent},
  {path: 'specialites', component: SpecialitesComponent},
  {path: 'treatment-tooth-decay', component: TreatmentToothDecayComponent},
  {path: 'paras-health-app', component: ParasHealthAppComponent},
  {path: 'hospital-page', component: HospitalPageComponent},
  {path: 'buttons', component: ButtonsComponent},
  {path: 'directions', component: DirectionsComponent},
  {path: 'special-clinic', component: SpecialClinicComponent},
  {path: 'treatment-checkup', component: TreatmentCheckupComponent},
  {path: 'treatment-child-health-checkup',component: TreatmentChildHealthCheckupComponent},
  {path: 'faq', component: FaqComponent},
  {path: 'infographics', component: InfographicsComponent},
  {path: 'infographics-video', component: InfographicsVideoComponent},
  {path: 'contact-us', component: ContactUsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
