import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-coe',
  templateUrl: './coe.component.html',
  styleUrls: ['./coe.component.css']
})
export class CoeComponent implements OnInit {

  constructor() { }

  customTitle: any = {};
  customImage:any = {};

  ngOnInit(): void {
    this.customTitle = "Department of Neurology"
    this.customImage = "assets/images/w40l838o.png"

    
  }


}
