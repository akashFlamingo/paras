import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-special-clinic',
  templateUrl: './special-clinic.component.html',
  styleUrls: ['./special-clinic.component.css']
})
export class SpecialClinicComponent implements OnInit {

  constructor() { }
  customTitle: any = {};
  customImage:any = {};
  customColor: any = {}
  
  
  
  

  ngOnInit(): void {
    this.customTitle = "Special clinics"
    this.customImage = "assets/images/special-clinic.png"
    this.customColor = "#FFD100"
    
  
   
  }

}