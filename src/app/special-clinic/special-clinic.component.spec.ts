import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialClinicComponent } from './special-clinic.component';

describe('SpecialClinicComponent', () => {
  let component: SpecialClinicComponent;
  let fixture: ComponentFixture<SpecialClinicComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpecialClinicComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialClinicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
