import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoeFaqComponent } from './coe-faq.component';

describe('CoeFaqComponent', () => {
  let component: CoeFaqComponent;
  let fixture: ComponentFixture<CoeFaqComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoeFaqComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoeFaqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
