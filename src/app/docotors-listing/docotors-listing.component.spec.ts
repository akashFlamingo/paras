import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocotorsListingComponent } from './docotors-listing.component';

describe('DocotorsListingComponent', () => {
  let component: DocotorsListingComponent;
  let fixture: ComponentFixture<DocotorsListingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocotorsListingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocotorsListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
