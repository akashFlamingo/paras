import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-docotors-listing',
  templateUrl: './docotors-listing.component.html',
  styleUrls: ['./docotors-listing.component.css']
})
export class DocotorsListingComponent implements OnInit {

  constructor() { }

  customTitle: any = {};

  ngOnInit(): void {
    this.customTitle = "Our Medical Experts"
  }

}
