import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutGroupCompaniesComponent } from './about-group-companies.component';

describe('AboutGroupCompaniesComponent', () => {
  let component: AboutGroupCompaniesComponent;
  let fixture: ComponentFixture<AboutGroupCompaniesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AboutGroupCompaniesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutGroupCompaniesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
