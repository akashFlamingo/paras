import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about-group-companies',
  templateUrl: './about-group-companies.component.html',
  styleUrls: ['./about-group-companies.component.css']
})
export class AboutGroupCompaniesComponent implements OnInit {

  constructor() { }

  customTitle: any = {};

  ngOnInit(): void {
    this.customTitle = "Group Companies"
  }

}
